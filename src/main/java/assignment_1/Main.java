package assignment_1;

import org.apache.hadoop.conf.Configuration;

public class Main {
    public static void main(String[] args) throws Exception {
        //----------Create 100 csv Files
        CsvFileGeneration csv=new CsvFileGeneration();
        csv.GenerateCSVs();

        //----------Upload Files from Local to HDFS
        HDFS_Converter lth=new HDFS_Converter();
        lth.copyFromLocal();

        //-----------Read Files from HDFS
        HBaseUtils hbase=new HBaseUtils();
        hbase.insertCSVDataToHbase(hdfsDestination,conf);
    }
}

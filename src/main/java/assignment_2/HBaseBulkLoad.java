package assignment_2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.tool.LoadIncrementalHFiles;
import java.io.IOException;

public class HBaseBulkLoad {
    public void doBulkLoad(String pathToHFile, String targetTableName) {
        try {
            Configuration conf = HBaseConfiguration.create();
            Connection connection = ConnectionFactory.createConnection(conf);

            Admin admin = connection.getAdmin();
            TableName tableName = TableName.valueOf(targetTableName);
            Table table = connection.getTable(tableName);
            LoadIncrementalHFiles load = new LoadIncrementalHFiles(conf);
            load.doBulkLoad(new Path(pathToHFile), admin, table, connection.getRegionLocator(tableName));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}


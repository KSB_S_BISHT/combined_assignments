package assignment_3;

import protoPackage.EmployeeProto;
import java.io.FileInputStream;
import java.io.IOException;

//Employee Class
public class Employee {
    public static void printEmployeeData(String path) throws IOException {
        EmployeeProto.EmployeeDatabase deserialized = EmployeeProto.EmployeeDatabase.newBuilder()
                .mergeFrom(new FileInputStream(path)).build();
        int empCount = deserialized.getEmployeeCount();
        for (int i = 0; i < empCount; i++) {
            System.out.println(deserialized.getEmployee(i).getAllFields());
        }
    }
}

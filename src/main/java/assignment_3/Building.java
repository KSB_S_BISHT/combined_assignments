package assignment_3;

import protoPackage.BuildingProto;

import java.io.FileInputStream;
import java.io.IOException;

public class Building {
    public static void printBuildingData(String path) throws IOException {
        BuildingProto.BuildingDatabase deserialized = BuildingProto.BuildingDatabase.newBuilder()
                .mergeFrom(new FileInputStream(path)).build();
        int empCount = deserialized.getBuildingCount();
        for (int i = 0; i < empCount; i++) {
            System.out.println(deserialized.getBuilding(i).getAllFields());
        }
    }
}

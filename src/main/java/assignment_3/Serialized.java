package assignment_3;

import java.io.IOException;

public class Serialized {
    public void serializeCSVFiles(String type, String csvFilePath, String serializedFileOutputPath) throws IOException {
        ProtoBfUtilities protoObj = new ProtoBfUtilities(csvFilePath, serializedFileOutputPath);
        if (type.equals("Employee")) {
            protoObj.serializeEmployeeCSV();
        } else if (type.equals("Building")) {
            protoObj.serializeBuildingCSV();
        } else {
            System.out.println("Please provide correct type: Employee / Building");
        }
    }
}
